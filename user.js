'use strict';
module.exports = function (sequelize, DataTypes) {
    let User = sequelize.define("User", {
        username:  {type: DataTypes.STRING, unique: true},
        passhash: DataTypes.STRING
    });
    return User;
};