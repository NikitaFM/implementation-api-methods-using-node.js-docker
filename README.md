Execute this command in project dir to build test task:
	$ docker build --tag doit/node-hapitesttask .

Execute these commands in order to run test task:
	$ docker run --name mysql-hapitesttask -e MYSQL_ROOT_PASSWORD=pass -e MYSQL_DATABASE=hapitestdb -d mysql:latest
	$ docker run --name mongo-hapitesttask -d mongo:latest
	$ docker run --name hapitesttask -p 80:80 --link mysql-hapitesttask:mysql --link mongo-hapitesttask:mongo doit/node-hapitesttask


