FROM node:4.4.0
COPY . .
RUN npm install
EXPOSE 80

CMD ["npm", "start"]
