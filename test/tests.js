'use strict';
const Code = require('code');
const Lab = require('lab');
const lab = exports.lab = Lab.script();
const server = require('../app.js');

const user = {
    username: 'test',
    password: 'foobar'
};

const product = {
    name: 'Table',
    description: 'Wooden table',
    price: 55.55,
    barcode: '48434543',
    category: 'furniture'
};

lab.experiment('Auth -', function () {
    let cookie = '';

    lab.before(function (done) {
        server.ext({
            type: 'onPostStart',
            method: function () {
                console.log('postStarted');
                done();
                return;
        }});
        setTimeout(function () {done();}, 30000);
    });

    lab.test('Insert product. Unauth', function (done) {
        let options = {
            method: 'POST',
            url: '/insertproduct',
            payload: product,
        };

        server.inject(options, function (response) {
            Code.expect(response.statusCode).to.equal(401);
            done();
        });
    });

    lab.test('Get products. Unauth', function (done) {
        let options = {
            method: 'GET',
            url: '/getproducts',
        };

        server.inject(options, function (response) {
            Code.expect(response.statusCode).to.equal(401);
            done();
        });
    });

    lab.test('Signing Up', function (done) {
        let options = {
            method: 'POST',
            url: '/signup',
            //payload: user
            credentials: user
        };

        server.inject(options, function (response) {
            Code.expect(response.statusCode).to.equal(200);
            done();
        });
    });

    lab.test('Signing Up with same username', function (done) {
        let options = {
            method: 'POST',
            url: '/signup',
            payload: {
                username: 'test',
                password: 'foobar'
            }
        };

        server.inject(options, function (response) {
            Code.expect(response.statusCode).to.equal(400);
            done();
        });
    });

    lab.test('Signing In with just created user credentials', function (done) {
        let options = {
            method: 'POST',
            url: '/signin',
            payload: user,
        };

        server.inject(options, function (response) {
            const header = response.headers['set-cookie'];
            cookie = header[0].match(/(?:[^\x00-\x20\(\)<>@\,;\:\\"\/\[\]\?\=\{\}\x7F]+)\s*=\s*(?:([^\x00-\x20\"\,\;\\\x7F]*))/);
            Code.expect(response.statusCode).to.equal(200);
            done();
        });
    });

    lab.test('Insert product', function (done) {
        let options = {
            method: 'POST',
            url: '/insertproduct',
            payload: product,
            headers: { cookie: 'testsid=' + cookie[1] }
        };

        server.inject(options, function (response) {
            Code.expect(response.statusCode).to.equal(200);
            done();
        });
    });

    lab.test('Get products', function (done) {
        let options = {
            method: 'GET',
            url: '/getproducts',
            headers: { cookie: 'testsid=' + cookie[1] },
        };

        server.inject(options, function (response) {
            console.dir(response);
            Code.expect(response.statusCode).to.equal(200);
            Code.expect(response.result).to.be.an.array().and.to.deep.include(product);
            done();
        });
    });

    lab.test('Signing out', function (done) {
        let options = {
            method: 'GET',
            url: '/signout',
            headers: { cookie: 'testsid=' + cookie[1] },
        };

        server.inject(options, function (response) {
            Code.expect(response.statusCode).to.equal(200);
            done();
        });
    });
});
