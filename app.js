'use strict';
const Hapi = require('hapi');
const uuid = require('uuid');
const path = require('path');
const crypto = require('crypto');
const Joi = require('joi');
const Boom = require('boom');

function signup(request, reply) {
    if (!request.payload.username || !request.payload.password) {
        return reply(Boom.badRequest('Missing username or password'));
    }
    const User = request.models.User;
    User.findAll({where: {username: request.payload.username}})
    .then(function (accounts) {
        if (accounts.length) {
            return reply(Boom.badRequest('Not unique'));
        }
        let hash = crypto.createHash('sha256');
        hash.update(request.payload.password);
        User.create({username: request.payload.username, passhash: hash.digest('base64')});
        reply({statusCode: 200, message: 'Account created'});
    });
}

function signin(request, reply) {
    if (request.auth.isAuthenticated) {
        return reply({statusCode: 200, message: 'Already signed in'});
    }
    let account;
    if (!request.payload.username || !request.payload.password) {
        return reply(Boom.badRequest('Missing username or password'));
    }
    else {
        request.models.User.findAll({where: {username: request.payload.username}}).then(function (accounts){
            const account = accounts[0];
            let hash = crypto.createHash('sha256');
            hash.update(request.payload.password);
            if (!account || account.passhash !== hash.digest('base64')) {
                return reply(Boom.unauthorized('Invalid username or password'));
            }

            const sid = uuid.v1();
            request.server.app.cache.set(sid, {account: account}, 0, function (err) {
                if (err) {
                    return reply(err);
                }
                request.cookieAuth.set({sid: sid});
                reply({statusCode: 200, message: 'Signin success. Welcome ' + account.username});
            });
        });
    }
}

function signout(request, reply) {
    request.cookieAuth.clear();
    return reply({statusCode: 200, message: 'Signout success'});
}

function getproducts(request, reply) {
    let db = request.server.plugins['hapi-mongodb'].db;
    db.collection('products').find({}).toArray(function (err, items) {
        if (err)
            throw err;
        else
            reply(items.map(function (c) {
                delete c._id;
                return c;
            }));
    });
}

function insertproduct(request, reply) {
    let product = {};
    product.name = request.payload.name;
    product.description = request.payload.description;
    product.price = request.payload.price;
    product.barcode = request.payload.barcode;
    product.category = request.payload.category;

    let db = request.server.plugins['hapi-mongodb'].db;
    let newID = new request.server.plugins['hapi-mongodb'].ObjectID();
    db.collection('products').update({"_id": newID}, {"$set": product}, {"upsert": true});
    reply({statusCode: 200, message: 'Seems like insert was success'});
}

const server = new Hapi.Server();
server.connection({port: 80});

server.register([{
    register: require('hapi-sequelize'),
    options: {
	user: 'root',
	pass: process.env.MYSQL_ENV_MYSQL_ROOT_PASSWORD,
	host: process.env.MYSQL_PORT_3306_TCP_ADDR,
	port: process.env.MYSQL_PORT_3306_TCP_PORT,
	database: 'hapitestdb',
        models: 'user.js',
        dialect: 'mysql',
        sequelize: {
            define: {
                underscoredAll: true
            },
            storage: path.join(__dirname, 'db.sql')
        }
    }}],
    function(err) {
        if (err) {
            throw err;
        }
        let db = server.plugins['hapi-sequelize'].db;
        db.sequelize.sync().then(function() { console.log('Models synced'); });
        server.ext('onPreHandler', function(modelCollections) {
            return function(request, reply) {
                request.models = modelCollections;
                reply.continue();
            }
        }(server.plugins['hapi-sequelize'].db.sequelize.models));
    }
);

server.register({
    register: require('hapi-mongodb'),
    options: {
        url: 'mongodb://' + process.env.MONGO_PORT_27017_TCP_ADDR + ':' + process.env.MONGO_PORT_27017_TCP_PORT + '/hapitestdb'
    }}, function (err) {
    if (err) {
        console.error(err);
        throw err;
    }
});

server.register(require('hapi-auth-basic'), function (err) {
    server.auth.strategy('docauth', 'basic', { validateFunc: function (request, username, password, callback) {
        if (username === 'niktest' && password === 'qwerty')
            callback(null, true, {username: username});
        else
            callback(null, false);
    }});
});

const options = {
    auth: 'docauth',
    info: {
        title: 'Hapi test task'
    },
    tags: [
        {
            name:'auth',
            description: 'Auth endpoints'
        },
        {
            name: 'products',
            description: 'Products endpoints'
        }
    ],
    sortTags: 'name'
};

server.register([require('inert'), require('vision'),
    {
        'register': require('hapi-swagger'),
        'options': options
    }],
    function (err) {
        if (err)
            throw err;
    }
);


server.register(require('hapi-auth-cookie'), function (err) {
    if (err) {
        throw err;
    }
    const cache = server.cache({segment: 'sessions', expiresIn: 3 * 24 * 60 * 60 * 1000});
    server.app.cache = cache;

    server.auth.strategy('session', 'cookie', true, {
        password: 'very-secure-cookie-password-for-test-task',
        cookie: 'testsid',
        isSecure: false,
        validateFunc: function (request, session, callback) {
            cache.get(session.sid, function (err, cached) {
                if (err) {
                    return callback(err, false);
                }
                if (!cached) {
                    return callback(null, false);
                }
                return callback(null, true, cached.account);
            });
        }
    });

    server.route([
        {method: 'POST', path: '/signup',
            config: {
                handler: signup,
                description: 'Sign up',
                notes: ['Just sign up. Don\'t forget to provide username and password ;-)'],
                tags: ['api', 'auth'],
                auth: {mode: 'try'},
                validate: {
                    payload: Joi.object({
                        username: Joi.string(),
                        password: Joi.string()})
                }
            }
        },
        {method: 'POST', path: '/signin',
            config: {
                handler: signin,
                description: 'Sign in',
                notes: ['Just sign in. Don\'t forget to provide username and password ;-)'],
                tags: ['api', 'auth'],
                auth: {mode: 'try'},
                plugins: {'hapi-auth-cookie': {redirectTo: false}},
                validate: {
                    payload: Joi.object({
                        username: Joi.string(),
                        password: Joi.string()
                    })
                }
            }
        },
        {method: 'GET', path: '/signout',
            config: {
                handler: signout,
                description: 'Sign in',
                notes: ['Just sign out'],
                tags: ['api', 'auth'],
            }},
        {method: 'GET', path: '/getproducts',
            config: {
                handler: getproducts,
                description: 'Product list',
                notes: ['Returns an array of products. Authorization required'],
                tags: ['api', 'products'],
                auth: {mode: 'required'},
                plugins: {'hapi-auth-cookie': {redirectTo: false}}}},
        {method: 'POST', path: '/insertproduct',
            config: {
                handler: insertproduct,
                description: 'Insert new product',
                notes: ['Stores new product to MongoDB. Authorization required'],
                tags: ['api', 'products'],
                auth: {mode: 'required'},
                plugins: {'hapi-auth-cookie': {redirectTo: false}},
                validate: {
                    payload: Joi.object({
                        name: Joi.string(),
                        description: Joi.string(),
                        price: Joi.number(),
                        barcode: Joi.string(),
                        category: Joi.string(),
                    })
                }
            }
        },
    ]);

    server.start(function() {
        console.log("Server started", server.info.uri);
    });
});

module.exports = server;
